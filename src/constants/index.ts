type ACTIONS =
    | 'SHARE'
    | 'CHANGE_PHOTO'
    | 'REMOVE_PHOTO'
    | 'FAIL'
    | 'DELETE'
    | 'CANCEL'
    | 'TRY_AGAIN'
    | 'REFLECT'
    | 'COMPLETE';

const menuSuccess: {
    title: string;
    action: ACTIONS;
}[] = [
    {
        title: 'Share',
        action: 'SHARE',
    },
    {
        title: 'Change photo',
        action: 'CHANGE_PHOTO',
    },
    {
        title: 'Remove photo',
        action: 'REMOVE_PHOTO',
    },
    {
        title: 'Actually I failed',
        action: 'FAIL',
    },
    {
        title: 'Delete',
        action: 'DELETE',
    },
    {
        title: 'Cancel',
        action: 'CANCEL',
    },
];

const menuFail: {
    title: string;
    action: ACTIONS;
}[] = [
    {
        title: 'Try tomorrow',
        action: 'TRY_AGAIN',
    },
    {
        title: 'Reflect',
        action: 'REFLECT',
    },
    {
        title: 'Actually I completed',
        action: 'COMPLETE',
    },
    {
        title: 'Delete',
        action: 'DELETE',
    },
    {
        title: 'Cancel',
        action: 'CANCEL',
    },
];

export const MENU_SUCCESS = {
    BUTTONS: menuSuccess,
    DESTRUCTIVE_INDEX: 4,
    CANCEL_INDEX: 5,
};

export const MENU_FAIL = {
    BUTTONS: menuFail,
    DESTRUCTIVE_INDEX: 3,
    CANCEL_INDEX: 4,
};

export const DATE_FORMAT = 'iiiiii, MMM dd';

import { Dimensions } from 'react-native';

export interface Gradients {
    [s: string]: string[];
}

export const color = {
    primary: 'rgba(0,0,0,.6)',
    primaryLight: 'rgba(0,0,0,.3)',
    secondary: '#fff',
    danger: '#F86C69',
    dangerLight: '#FE5196',
    demo: '#F6CF55',
    success: '#72AFD3',
    successLight: '#37ECBA',
};

export const background = {
    primary: '#fff',
    secondary: '#CFE4EC',
};

export const gradient: Gradients = {
    default: [background.secondary, background.secondary],
    mask: ['rgba(0,0,0,0.08)', 'rgba(0,0,0,0.9)'],
    trial: [color.demo, color.danger],
    danger: [color.dangerLight, color.danger],
    success: [color.successLight, color.success],
};

const { width, height } = Dimensions.get('window');

export const layout = {
    window: {
        width,
        height,
    },
    isSmallDevice: width < 375,
    space: 16,
    borderRadius: 12,
};

export const font = {
    size: {
        default: 16,
        title: 24,
    },
};

import { createStackNavigator } from 'react-navigation';

import { AddNewScreen } from '../screens/AddNewScreen';
import { TimeLineScreen } from '../screens/TimeLineScreen';
import { RetroScreen } from '../screens/RetroScreen';

export const AppStack = createStackNavigator(
    {
        AddNew: AddNewScreen,
        TimeLine: TimeLineScreen,
        Retro: RetroScreen,
    },
    { initialRouteName: 'TimeLine' },
);

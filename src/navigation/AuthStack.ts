import { createStackNavigator } from 'react-navigation';

import { AuthScreen } from '../screens/AuthScreen';

export const AuthStack = createStackNavigator({
    Auth: AuthScreen,
});

import { Alert, Platform } from 'react-native';
import firebase from 'react-native-firebase';
import ImagePicker, { Image } from 'react-native-image-crop-picker';
// import Constants from 'expo-constants';
// import * as Permissions from 'expo-permissions';

// import { database, storage } from '../firebaseConfig';
// import { FirebaseError } from 'firebase';

// More info on all the options is below in the API Reference... just some common use cases shown here
const options = {
    title: 'Select Avatar',
    customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};

const pickImage = async () =>
    ImagePicker.openPicker({
        width: 300,
        height: 275,
        multiple: false,
        cropping: true,
    });

const getCameraRollPermissionAsync = async () => {
    if (Platform.OS === 'ios') {
        const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status !== 'granted') {
            Alert.alert('Sorry, we need camera roll permissions to make this work!');
        }
    }
};

interface UploadImageArgs {
    image: Image;
    userId: string;
}

const uploadImage = async (
    { image, userId }: UploadImageArgs,
    progressCallback?: (progress: number) => void,
) => {
    const imageName = String(Number(new Date()));
    const ref = firebase.storage().ref(`cards/${userId}/${imageName}`);
    return new Promise((resolve: (url: string) => void) => {
        ref.putFile(image.path).on(
            // @ts-ignore
            firebase.storage.TaskEvent.STATE_CHANGED,
            ({ bytesTransferred, totalBytes, state, ref }) => {
                progressCallback &&
                    progressCallback(Math.floor((bytesTransferred / totalBytes) * 100));
                // @ts-ignore
                if (state === firebase.storage.TaskState.SUCCESS) {
                    ref.getDownloadURL().then(resolve);
                }
            },
            () => {
                Alert.alert('Error, ply try again');
            },
        );
    });
};

interface AddPhotoArgs {
    userId: string;
    cardId: string;
}

export const addPhoto = async (
    { userId, cardId }: AddPhotoArgs,
    progressCallback?: (progress: number) => void,
) => {
    const image = await pickImage();
    // TODO: get from Camera
    // await getCameraRollPermissionAsync();

    // fix TS Array.isArray(image)
    if (!image || Array.isArray(image)) {
        return false;
    }
    const { height, width } = image;
    const uploadedImageUri = await uploadImage({ image, userId }, progressCallback);
    firebase
        .database()
        .ref()
        .child(`cards/${userId}/${cardId}`)
        .update({
            image: {
                height,
                width,
                uri: uploadedImageUri,
            },
        });
    return false;
};

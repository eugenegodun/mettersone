import AsyncStorage from '@react-native-community/async-storage';

export const getUserIdAsync = async () => {
    const userData = await AsyncStorage.getItem('userData');
    if (!userData) {
        return null;
    }
    const userObj = JSON.parse(userData!);
    // REVIEW: set UserId to state?
    return userObj.uid;
};

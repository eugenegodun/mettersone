import { Share, Alert, ShareContent } from 'react-native';

export const share = async ({ title, message }: ShareContent) => {
    const { share, sharedAction } = Share;
    console.log(title, message)
    try {
        const result = await share({
            title,
            message,
        });
        if (result.action === sharedAction) {
            if (result.activityType) {
                // shared with activity type of result.activityType
                Alert.alert(result.activityType);
            } else {
                Alert.alert('Shared!');
                // shared
            }
        }
    } catch (error) {
        Alert.alert('Something went wrond, try again');
    }
};

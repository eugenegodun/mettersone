import React, { useState } from 'react';
// import * as Icon from '@expo/vector-icons';
// import { useScreens } from 'react-native-screens';
// import { AppLoading } from 'expo';
// import { Asset } from 'expo-asset';
// import * as Font from 'expo-font';
import { Platform, StatusBar, StyleSheet, View, Text } from 'react-native';
import AppNavigator from './navigation/AppNavigator';

// TODO: uncomment
// useScreens();

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

const Root = () => {
    // const [isLoadingComplete, setLoadingComplete] = useState(false);
    // const loadResourcesAsync = async () => {
    //     return Promise.all([
    //         Asset.loadAsync([
    //             require('./assets/images/robot-dev.png'),
    //             require('./assets/images/robot-prod.png'),
    //         ]),
    //         Font.loadAsync({
    //             // This is the font that we are using for our tab bar
    //             ...Icon.Ionicons.font,
    //             // We include SpaceMono because we use it in HomeScreen.js. Feel free
    //             // to remove this if you are not using it in your app
    //             'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
    //         }),
    //     ]);
    // };

    // const handleLoadingError = (error: Error) => {
    //     // In this case, you might want to report the error to your error
    //     // reporting service, for example Sentry
    //     console.warn(error);
    // };

    // const handleFinishLoading = () => {
    //     setLoadingComplete(true);
    // };

    // if (!isLoadingComplete && !skipLoadingScreen) {
    //     return (
    //         <AppLoading
    //             startAsync={loadResourcesAsync}
    //             onError={handleLoadingError}
    //             onFinish={handleFinishLoading}
    //         />
    //     );
    // }
    return (
        <View style={styles.container}>
            {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
            <AppNavigator />
        </View>
    );
};

export { Root };

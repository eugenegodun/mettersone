import { ImageURISource } from 'react-native';

export type Status = 'success' | 'fail';

export type MainButttonActionType = 'share' | 'plan' | 'buy' | 'try' | 'complete' | 'fail';

export interface Data {
    date: number;
    id: string;
    image?: ImageURISource;
    title: string;
    body?: string;
    retro?: string;
    success?: boolean;
    done: boolean;
}

export type ThemeType = 'light' | 'dark' | 'danger';

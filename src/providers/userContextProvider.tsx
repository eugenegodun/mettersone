import { createContext } from 'react';

// Signed-in user context
export const UserContext = createContext({
    userId: '',
});

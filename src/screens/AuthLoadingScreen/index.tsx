import React, { useEffect } from 'react';
import { withNavigation, NavigationScreenProps } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';

import { FullScreenLoader } from '../../components/FullScreenLoader';

const AuthLoadingScreenDumb = ({ navigation }: NavigationScreenProps) => {
    useEffect(() => {
        bootstrapAsync();
    }, []);
    const bootstrapAsync = async () => {
        // await AsyncStorage.removeItem('userData');
        const userData = await AsyncStorage.getItem('userData');
        navigation.navigate(userData ? 'App' : 'Auth');
    };
    return <FullScreenLoader />;
};

export const AuthLoadingScreen = withNavigation(AuthLoadingScreenDumb);

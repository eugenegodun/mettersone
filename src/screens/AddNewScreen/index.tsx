import React from 'react';
import LinearGradient from 'react-native-linear-gradient';

import { AddNew } from '../../components/AddNew';
import { Close } from '../../components/Close';
import { gradient } from '../../constants/styles';
import { styles } from './styles';

const AddNewScreen = () => (
    <LinearGradient style={styles.container} colors={gradient.danger}>
        <AddNew />
        <Close />
    </LinearGradient>
);

export { AddNewScreen };

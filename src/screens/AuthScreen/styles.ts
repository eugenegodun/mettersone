import { StyleSheet, TextStyle, ViewStyle } from 'react-native';

import { background } from '../../constants/styles';
import { styles as coreStyles } from '../styles';

interface Style {
    container: ViewStyle;
    button: ViewStyle;
    buttonText: TextStyle;
}

export const styles = StyleSheet.create<Style>({
    container: {
        ...coreStyles.container,
        backgroundColor: background.primary,
        alignItems: 'center',
        justifyContent: 'center',
    },
    button: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        paddingHorizontal: 8,
        height: 40,
        backgroundColor: '#4267b2',
        borderRadius: 5,
    },
    buttonText: {
        marginLeft: 6,
        fontSize: 18,
        fontWeight: '500',
        color: '#fff',
    },
});

import React, { useState } from 'react';
import { View, TouchableOpacity, Text, Alert, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { AccessToken, LoginManager } from 'react-native-fbsdk';
import firebase from 'react-native-firebase';
// import Constants from 'expo-constants';
// import * as Facebook from 'expo-facebook';

import { styles } from './styles';
// import { Ionicons } from '@expo/vector-icons';
import { NavigationScreenProps } from 'react-navigation';
import { color } from '../../constants/styles';

const getUserById = async (userId: string) =>
    new Promise(resolve => {
        firebase
            .database()
            .ref()
            .child(`users/${userId}`)
            .once('value', snap => {
                resolve(snap.val());
            });
    });

const createNewUser = async ({ uid, displayName, email, photoURL }) =>
    firebase
        .database()
        .ref()
        .child('users')
        .child(uid)
        .set({
            uid,
            email,
            photoURL,
            provider: 'facebook',
            name: displayName,
        });

export const AuthScreen = ({ navigation }: NavigationScreenProps) => {
    const [isLoading, setIsLoading] = useState(false);
    const signInWithFacebook = async () => {
        setIsLoading(true);
        try {
            const result = await LoginManager.logInWithPermissions(['public_profile', 'email']);

            if (result.isCancelled) {
                // handle this however suites the flow of your app
                throw new Error('User cancelled request');
            }
            if (result && result.grantedPermissions) {
                console.log(
                    `Login success with permissions: ${result.grantedPermissions.toString()}`,
                );
            }

            // get the access token
            const data = await AccessToken.getCurrentAccessToken();

            if (!data) {
                // handle this however suites the flow of your app
                throw new Error('Something went wrong obtaining the users access token');
            }

            // create a new firebase credential with the token
            const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);

            // login with credential
            const firebaseUserCredential = await firebase.auth().signInWithCredential(credential);
            if (!firebaseUserCredential) {
                return Promise.reject({ type: 'cancel' });
            }
            const user = await getUserById(firebaseUserCredential.user.uid);
            if (user) {
                await AsyncStorage.setItem('userData', JSON.stringify(user));
            } else {
                await createNewUser(firebaseUserCredential.user);
            }
            setIsLoading(false);
            navigation.navigate('App');
            return Promise.resolve({ type: 'success' });
        } catch (e) {
            console.error(e);
        }
    };
    return (
        <View style={styles.container}>
            {isLoading ? (
                <ActivityIndicator size="large" color={color.danger} />
            ) : (
                <TouchableOpacity onPress={signInWithFacebook} style={styles.button}>
                    {/* <Ionicons name="logo-facebook" size={24} color={color.secondary} /> */}
                    <Text style={styles.buttonText}>Log in With Facebook</Text>
                </TouchableOpacity>
            )}
        </View>
    );
};

import { StyleSheet } from 'react-native';

import { background } from '../../constants/styles';
import { styles as coreStyles } from '../styles';

export const styles = StyleSheet.create({
    container: {
        ...coreStyles.container,
        backgroundColor: background.primary,
    },
});

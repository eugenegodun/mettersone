import React, { useEffect, useState } from 'react';
import { FlatList } from 'react-native';
import firebase from 'react-native-firebase';

import { CardFuture } from '../../components/Card/CardFuture';
import { CardImage } from '../../components/Card/CardImage';
import { CardPrimary } from '../../components/Card/CardPrimary';
import { CardTrial } from '../../components/Card/CardTrial';
import { Data } from '../../types';
import { styles } from './styles';
import { FullScreenLoader } from '../../components/FullScreenLoader';
import { getUserIdAsync } from '../../utils/getUserIdAsync';
import { UserContext } from '../../providers/userContextProvider';

const TimeLineScreen = () => {
    const [data, setData] = useState([] as Data[]);
    const [userId, setUserId] = useState('');
    const [isLoading, setIsLoading] = useState(true);
    const getCards = async () => {
        const userId = await getUserIdAsync();
        firebase
            .database()
            .ref()
            .child(`cards/${userId}`)
            .orderByKey()
            .limitToFirst(7)
            .on('value', snap => {
                const dataObj = snap.val();
                const data: Data[] = [];
                for (const key in dataObj) {
                    data.push(dataObj[key]);
                }
                setData(data.reverse());
                setUserId(userId);
                setIsLoading(false);
            });
    };
    useEffect(() => {
        getCards();
    }, []);
    if (isLoading || !userId) {
        return <FullScreenLoader />;
    }
    return (
        <UserContext.Provider value={{ userId }}>
            <FlatList
                style={[styles.container]}
                data={data}
                renderItem={({ item }: { item: Data }) =>
                    item.image ? (
                        <CardImage {...{ userId, data: item }} />
                    ) : (
                        <CardPrimary {...{ userId, data: item }} />
                    )
                }
                keyExtractor={({ id }) => String(id)}
                ListHeaderComponent={() => (
                    <>
                        {/* <CardTrial /> */}
                        <CardFuture />
                    </>
                )}
            />
        </UserContext.Provider>
    );
};

export { TimeLineScreen };

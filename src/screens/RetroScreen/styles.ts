import { StyleSheet, ViewStyle } from 'react-native';
import { background } from '../../constants/styles';

interface Style {
    container: ViewStyle;
}

export const styles = StyleSheet.create<Style>({
    container: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: background.secondary,
    },
});

import React from 'react';

import { Retro } from '../../components/Retro';
import { Close } from '../../components/Close';
import { styles } from './styles';
import { View } from 'react-native';

export const RetroScreen = () => (
    <View style={styles.container}>
        <Retro />
        <Close />
    </View>
);

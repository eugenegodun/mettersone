import { StyleSheet } from 'react-native';

import { layout } from '../constants/styles';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginVertical: layout.space,
        marginHorizontal: layout.space,
    },
});

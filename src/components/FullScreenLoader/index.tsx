import React from 'react';
import { ActivityIndicator, View } from 'react-native';

import { styles } from './styles';
import { color } from '../../constants/styles';

export const FullScreenLoader = () => (
    <View style={styles.container}>
        <ActivityIndicator size="large" color={color.danger} />
    </View>
);

import { StyleSheet, ViewStyle } from 'react-native';
import { color } from '../../../constants/styles';

interface Style {
    button: ViewStyle;
    dot: ViewStyle;
    dark: ViewStyle;
    light: ViewStyle;
    dots: ViewStyle;
}

export const styles = StyleSheet.create<Style>({
    dots: {
        position: 'absolute',
        right: 0,
        top: 0,
        bottom: 0,
        width: 64,
    },
    button: {
        display: 'flex',
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    dot: {
        display: 'flex',
        marginHorizontal: 2,
        width: 4,
        height: 4,
        borderRadius: 2,
    },
    dark: {
        backgroundColor: color.primary,
    },
    light: {
        backgroundColor: color.secondary,
    },
});

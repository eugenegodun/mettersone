import React, { useContext } from 'react';
import { ActionSheetIOS, TouchableOpacity, View, ViewStyle } from 'react-native';
import { withHandlers, compose } from 'recompose';
import { withNavigation, NavigationScreenProps } from 'react-navigation';
import firebase from 'react-native-firebase';

import { MENU_SUCCESS, MENU_FAIL } from '../../../constants';
import { MainButttonActionType, ThemeType } from '../../../types';
import { styles } from './styles';
import { addPhoto } from '../../../utils/addPhoto';
import { UserContext } from '../../../providers/userContextProvider';

interface OuterProps {
    style?: ViewStyle;
    action: MainButttonActionType;
    type: ThemeType;
    cardId: string;
}
interface InnerProps {
    onPress: (userId: string) => void;
    navigation: NavigationScreenProps;
}

const CardMoreDumb = ({ style, onPress, type }: OuterProps & InnerProps) => {
    const dotStyle = [styles.dot, styles[type]];
    const { userId } = useContext(UserContext);
    return (
        <View style={[styles.dots]}>
            <TouchableOpacity onPress={() => onPress(userId)} style={[styles.button, style]}>
                <View style={dotStyle} />
                <View style={dotStyle} />
                <View style={dotStyle} />
            </TouchableOpacity>
        </View>
    );
};

export const CardMore = compose<OuterProps, InnerProps>(
    withNavigation,
    withHandlers({
        onPress: ({ action, cardId, navigation }) => (userId: string) => {
            let menuType = MENU_SUCCESS;
            switch (action) {
                case 'share':
                    menuType = MENU_SUCCESS;
                    break;
                case 'try':
                    menuType = MENU_FAIL;
                    break;
                default:
                    menuType = MENU_SUCCESS;
            }
            const { BUTTONS, CANCEL_INDEX, DESTRUCTIVE_INDEX } = menuType;
            ActionSheetIOS.showActionSheetWithOptions(
                {
                    options: BUTTONS.map(button => button.title),
                    cancelButtonIndex: CANCEL_INDEX,
                    destructiveButtonIndex: DESTRUCTIVE_INDEX,
                },
                buttonIndex => {
                    switch (BUTTONS[buttonIndex].action) {
                        case 'SHARE':
                            // TODO: share
                            break;
                        case 'CHANGE_PHOTO':
                            // TODO: loader
                            addPhoto({ userId, cardId });
                            break;
                        case 'REMOVE_PHOTO':
                            firebase
                                .database()
                                .ref()
                                .child(`cards/${userId}/${cardId}/image`)
                                .remove();
                            break;
                        case 'DELETE':
                            firebase
                                .database()
                                .ref()
                                .child(`cards/${userId}/${cardId}`)
                                .remove();
                            break;
                        case 'FAIL':
                            firebase
                                .database()
                                .ref()
                                .child(`cards/${userId}/${cardId}`)
                                .update({
                                    success: false,
                                });
                            break;
                        case 'REFLECT':
                            navigation.navigate('Retro', {
                                cardId,
                                userId,
                            });
                            break;
                        case 'TRY_AGAIN':
                            firebase
                                .database()
                                .ref()
                                .child(`cards/${userId}/${cardId}`)
                                .update({
                                    done: false,
                                    success: null,
                                    retro: null,
                                });
                            break;
                        case 'COMPLETE':
                            firebase
                                .database()
                                .ref()
                                .child(`cards/${userId}/${cardId}`)
                                .update({
                                    success: true,
                                    retro: null,
                                });
                            break;
                        default:
                            return false;
                    }
                },
            );
        },
    }),
)(CardMoreDumb);

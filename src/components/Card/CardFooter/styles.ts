import { StyleSheet, ViewStyle } from 'react-native';

interface Style {
    footer: ViewStyle;
    light: ViewStyle;
    dark: ViewStyle;
}

export const styles = StyleSheet.create<Style>({
    footer: {
        display: 'flex',
        flex: 1,
        alignItems: 'stretch',
        flexDirection: 'row',
        borderTopWidth: 1,
    },
    dark: {
        borderTopColor: 'rgba(0,0,0, .04)',
    },
    light: {
        borderTopColor: 'rgba(255,255,255, .16)',
    },
});

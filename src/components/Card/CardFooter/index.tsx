import React, { ReactElement } from 'react';
import { View } from 'react-native';
import { ThemeType } from '../../../types';
import { styles } from './styles';

interface OuterProps {
    children: ReactElement;
    type: ThemeType;
}

export const CardFooter = ({ children, type }: OuterProps) => (
    <View style={[styles.footer, styles[type]]}>{children}</View>
);

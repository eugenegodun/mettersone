import { StyleSheet, TextStyle, ViewStyle } from 'react-native';

import { color } from '../../../constants/styles';

interface Style {
    button: ViewStyle;
    buttonText: TextStyle;
    light: TextStyle;
    dark: TextStyle;
    danger: TextStyle;
}

export const styles = StyleSheet.create<Style>({
    button: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        width: 100,
    },
    buttonText: {
        fontWeight: 'bold',
        fontSize: 16,
    },
    light: {
        color: color.secondary,
    },
    dark: {
        color: color.primary,
    },
    danger: {
        color: color.danger,
    },
});

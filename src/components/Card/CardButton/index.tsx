import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

import { ThemeType } from '../../../types';
import { styles } from './styles';

interface OuterProps {
    title: string;
    type: ThemeType;
    onPress: () => void;
}

export const CardButton = ({ title, type, onPress }: OuterProps) => (
    <TouchableOpacity onPress={onPress} style={styles.button}>
        <Text style={[styles.buttonText, styles[type]]}>{title}</Text>
    </TouchableOpacity>
);

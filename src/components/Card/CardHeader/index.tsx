import React, { ReactElement } from 'react';
import { View } from 'react-native';
import { CardBox } from '../CardBox';
import { styles } from './styles';

interface OuterProps {
    children: ReactElement;
}

export const CardHeader = ({ children }: OuterProps) => (
    <View style={styles.header}>{children}</View>
);

import { StyleSheet, ViewStyle } from 'react-native';
import { layout } from '../../../constants/styles';

interface Style {
    header: ViewStyle;
}

export const styles = StyleSheet.create<Style>({
    header: {
        flex: 1,
        display: 'flex',
        justifyContent: 'flex-end',
        paddingHorizontal: layout.space,
        paddingBottom: layout.space,
    },
});

import { ImageStyle, StyleSheet, ViewStyle } from 'react-native';
import { layout } from '../../../constants/styles';

interface Style {
    container: ViewStyle;
    imageContainer: ViewStyle;
    image: ImageStyle;
    gradient: ViewStyle;
}

export const styles = StyleSheet.create<Style>({
    container: {
        flex: 1,
    },
    gradient: {
        flex: 1,
        borderRadius: layout.borderRadius,
    },
    imageContainer: {
        flex: 1,
    },
    image: {
        borderRadius: layout.borderRadius,
        resizeMode: 'cover',
    },
});

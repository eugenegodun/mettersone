import LinearGradient from 'react-native-linear-gradient';
import { format } from 'date-fns';
import React from 'react';
import { ImageBackground, View } from 'react-native';
import { compose, withHandlers } from 'recompose';

import { gradient } from '../../../constants/styles';
import { Data } from '../../../types';
import { CardBody } from '../CardBody';
import { CardButton } from '../CardButton';
import { CardContainer } from '../CardContainer';
import { CardFooter } from '../CardFooter';
import { CardHeader } from '../CardHeader';
import { CardCore } from '../CardCore';
import { CardMore } from '../CardMore';
import { CardText } from '../CardText';
import { CardTitle } from '../CardTitle';
import { styles } from './styles';
import { share } from '../../../utils/share';
import { DATE_FORMAT } from '../../../constants';

interface OuterProps {
    data: Data;
}

interface InnerProps {
    onPressShare: () => void;
}

const CardImageDumb = ({
    data: { date, title, body, image, id },
    onPressShare,
}: OuterProps & InnerProps) => (
    <CardContainer>
        <View style={styles.container}>
            <ImageBackground
                style={styles.imageContainer}
                imageStyle={styles.image}
                source={{ uri: image!.uri }}
                resizeMode="cover"
            >
                <LinearGradient style={styles.gradient} colors={gradient.mask}>
                    <CardCore>
                        <>
                            <CardHeader>
                                <CardTitle type="light">
                                    {format(new Date(date), DATE_FORMAT)}
                                </CardTitle>
                            </CardHeader>
                            <CardBody>
                                <>
                                    <CardText type="light">{title}</CardText>
                                    {body && <CardText type="light">{body}</CardText>}
                                </>
                            </CardBody>
                            <CardFooter type="light">
                                <>
                                    <CardButton
                                        {...{
                                            onPress: onPressShare,
                                            type: 'light',
                                            title: 'Share',
                                        }}
                                    />
                                    <CardMore {...{ cardId: id, type: 'light', action: 'share' }} />
                                </>
                            </CardFooter>
                        </>
                    </CardCore>
                </LinearGradient>
            </ImageBackground>
        </View>
    </CardContainer>
);

export const CardImage = compose<InnerProps, OuterProps>(
    withHandlers({
        onPressShare: ({ data: { title, body } }) => () => {
            share({
                title,
                message: body,
            });
        },
        onPressMore: () => () => console.log('onPressMore'),
    }),
)(CardImageDumb);

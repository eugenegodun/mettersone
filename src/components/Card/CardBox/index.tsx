import React, { ReactElement } from 'react';
import { View, ViewStyle } from 'react-native';
import { styles } from './styles';

interface OuterProps {
    children: ReactElement;
    style?: ViewStyle;
}

export const CardBox = ({ children, style }: OuterProps) => (
    <View style={[styles.box, style]}>{children}</View>
);

import { StyleSheet, TextStyle } from 'react-native';

import { color } from '../../../constants/styles';

interface Style {
    title: TextStyle;
    light: TextStyle;
    dark: TextStyle;
}

export const styles = StyleSheet.create<Style>({
    title: {
        fontSize: 24,
        lineHeight: 28,
        fontWeight: 'bold',
    },
    light: {
        color: color.secondary,
    },
    dark: {
        color: color.primary,
    },
});

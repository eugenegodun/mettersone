import React from 'react';
import { Text } from 'react-native';

import { styles } from './styles';
import { ThemeType } from '../../../types';

interface OuterProps {
    children: string;
    type: ThemeType;
}

export const CardTitle = ({ children, type }: OuterProps) => (
    <Text style={[styles.title, styles[type]]}>{children}</Text>
);

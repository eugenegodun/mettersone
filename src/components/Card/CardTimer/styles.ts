import { StyleSheet, ViewStyle, TextStyle } from 'react-native';
import { color } from '../../../constants/styles';

interface Style {
    timer: ViewStyle;
    text: TextStyle;
}

export const styles = StyleSheet.create<Style>({
    timer: {
        display: 'flex',
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        fontWeight: 'bold',
        fontSize: 16,
        color: color.secondary,
    },
});

import React from 'react';
import { Text, View, Alert } from 'react-native';
import Countdown, { CountdownRenderProps } from 'react-countdown-now';
import { withHandlers } from 'recompose';
import firebase from 'react-native-firebase';

import { styles } from './styles';

interface OuterProps {
    dateEnd: Date;
    userId: string;
    cardId: string;
}

interface InnerProps {
    onComplete: () => {};
}

// TODO: counter
const CardTimerDumb = ({ dateEnd, onComplete }: OuterProps & InnerProps) => {
    // TODO: fix plugin
    const zeroPad = (value: number) => (value < 10 ? `0${value}` : value);
    const renderCounter = ({ hours, minutes, seconds }: CountdownRenderProps) => {
        return (
            <Text style={styles.text}>
                {zeroPad(hours)} h : {zeroPad(minutes)} min : {zeroPad(seconds)} sec
            </Text>
        );
    };
    return (
        <View style={styles.timer}>
            <Countdown
                date={Number(dateEnd) + 3000}
                onComplete={onComplete}
                renderer={renderCounter}
            />
        </View>
    );
};

export const CardTimer = withHandlers<OuterProps, {}>({
    onComplete: ({ userId, cardId }) => () => {
        Alert.alert('onComplete');
        firebase
            .database()
            .ref()
            .child(`cards/${userId}/${cardId}`)
            .update({
                done: true,
                success: null,
            });
    },
})(CardTimerDumb);

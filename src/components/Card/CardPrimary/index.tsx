import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import { compose, withProps } from 'recompose';
import { format } from 'date-fns';

import { CardBody } from '../CardBody';
import { CardContainer } from '../CardContainer';
import { CardFooter } from '../CardFooter';
import { CardHeader } from '../CardHeader';
import { CardCore } from '../CardCore';
import { CardText } from '../CardText';
import { CardTitle } from '../CardTitle';
import { Data, ThemeType } from '../../../types';
import { styles } from './styles';
import { CardPrimaryActions } from './CardPrimaryActions';
import { gradient } from '../../../constants/styles';
import { DATE_FORMAT } from '../../../constants';

interface OuterProps {
    data: Data;
    userId: string;
}

interface InnerProps {
    themeType: ThemeType;
    gradientColor: string[];
}

const CardPrimaryDumb = ({
    data: { date, title, body, done, success, id, retro },
    themeType,
    gradientColor,
    userId,
}: OuterProps & InnerProps) => {
    console.log('retro', retro);
    return (
        <CardContainer>
            <LinearGradient style={styles.cardPrimary} colors={gradientColor}>
                <CardCore>
                    <>
                        <CardHeader>
                            <CardTitle type={themeType}>
                                {format(new Date(date), DATE_FORMAT)}
                            </CardTitle>
                        </CardHeader>
                        <CardBody>
                            <>
                                <CardText type={themeType}>{title}</CardText>
                                {body ? <CardText type={themeType}>{body}</CardText> : null}
                                {done && retro ? (
                                    <CardText type={themeType}>{retro}</CardText>
                                ) : null}
                            </>
                        </CardBody>
                        <CardFooter type={themeType}>
                            <CardPrimaryActions {...{ userId, done, success, retro, cardId: id }} />
                        </CardFooter>
                    </>
                </CardCore>
            </LinearGradient>
        </CardContainer>
    );
};

export const CardPrimary = compose<OuterProps, InnerProps>(
    withProps(({ data: { success } }) => {
        let gradientColor: string[] = gradient.danger;
        let themeType: ThemeType = 'light';
        switch (success) {
            case true:
                gradientColor = gradient.success;
                themeType = 'light';
                break;
            case false:
                gradientColor = gradient.default;
                themeType = 'dark';
                break;
            default:
                gradientColor = gradient.danger;
                themeType = 'light';
                break;
        }
        return {
            gradientColor,
            themeType,
        };
    }),
)(CardPrimaryDumb);

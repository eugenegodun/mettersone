import React from 'react';
import firebase from 'react-native-firebase';

import { CardButton } from '../../../CardButton';
import { CardMore } from '../../../CardMore';
import { compose, withHandlers } from 'recompose';

interface OuterProps {
    cardId: string;
    userId: string;
    onPressTryAgain: () => void;
}

interface InnerProps {
    cardId: string;
}

const TryAgainDumb = ({ cardId, onPressTryAgain }: OuterProps & InnerProps) => {
    return (
        <>
            <CardButton type="dark" onPress={onPressTryAgain} title="Try tomorrow" />
            <CardMore {...{ cardId, type: 'dark', action: 'try' }} />
        </>
    );
};

export const TryAgain = compose<OuterProps, {}>(
    withHandlers({
        onPressTryAgain: ({ cardId, userId }) => () => {
            firebase
                .database()
                .ref()
                .child(`cards/${userId}/${cardId}`)
                .update({
                    done: false,
                    success: null,
                    retro: null,
                });
        },
    }),
)(TryAgainDumb);

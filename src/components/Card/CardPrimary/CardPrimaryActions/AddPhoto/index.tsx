import React, { useState, useContext } from 'react';

import { CardButton } from '../../../CardButton';
import { CardMore } from '../../../CardMore';
import { addPhoto } from '../../../../../utils/addPhoto';
import { UserContext } from '../../../../../providers/userContextProvider';

interface OuterProps {
    cardId: string;
}

export const AddPhoto = ({ cardId }: OuterProps) => {
    const [progress, setProgress] = useState(0);
    const { userId } = useContext(UserContext);
    return (
        <>
            <CardButton
                type="light"
                onPress={() => addPhoto({ userId, cardId }, setProgress)}
                title={progress ? `${progress} %` : 'Add Photo'}
            />
            {!progress && <CardMore {...{ cardId, type: 'light', action: 'share' }} />}
        </>
    );
};

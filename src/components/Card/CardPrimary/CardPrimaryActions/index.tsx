import React from 'react';
import { View } from 'react-native';
import { NavigationScreenProps, withNavigation } from 'react-navigation';
import { withHandlers, compose } from 'recompose';
import firebase from 'react-native-firebase';

import { CardButton } from '../../CardButton';
import { styles } from './styles';
import { CardTimer } from '../../CardTimer';
import { AddPhoto } from './AddPhoto';
import { TryAgain } from './TryAgain';

interface OuterProps {
    done: boolean;
    success?: boolean;
    retro?: string;
    cardId: string;
    userId: string;
}

interface InnerProps {
    onPressHandler: (success: boolean) => void;
    onPressReflect: () => void;
}

const CardPrimaryActionsDumb = ({
    done,
    success,
    retro,
    cardId,
    userId,
    onPressHandler,
    onPressReflect,
}: OuterProps & InnerProps) => {
    if (!done) {
        return <CardTimer {...{ cardId, userId, dateEnd: new Date() }} />;
    }
    if (done && retro) {
        return <TryAgain {...{ cardId, userId }} />;
    }
    if (done && success === undefined) {
        return (
            <>
                <View style={styles.action}>
                    <CardButton
                        {...{
                            type: 'light',
                            onPress: () => onPressHandler(true),
                            title: 'Completed',
                        }}
                    />
                </View>
                <View style={styles.action}>
                    <CardButton
                        {...{
                            type: 'light',
                            onPress: () => onPressHandler(false),
                            title: 'Failed',
                        }}
                    />
                </View>
            </>
        );
    }
    return success ? (
        <AddPhoto {...{ cardId }} />
    ) : (
        <CardButton {...{ type: 'dark', onPress: onPressReflect, title: 'Reflect' }} />
    );
};

export const CardPrimaryActions = compose<OuterProps, {}>(
    withNavigation,
    withHandlers({
        onPressHandler: ({ userId, cardId }) => (success: boolean) => {
            firebase
                .database()
                .ref()
                .child(`cards/${userId}/${cardId}`)
                .update({
                    success,
                });
        },
        onPressReflect: ({ navigation, cardId, userId }) => () => {
            navigation.navigate('Retro', {
                cardId,
                userId,
            });
        },
        onPressTryAgain: ({ cardId, userId }) => () => {
            firebase
                .database()
                .ref()
                .child(`cards/${userId}/${cardId}`)
                .update({
                    done: false,
                    success: null,
                });
        },
    }),
)(CardPrimaryActionsDumb);

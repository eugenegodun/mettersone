import { StyleSheet, ViewStyle } from 'react-native';

import { background, layout } from '../../../constants/styles';

interface Style {
    cardPrimary: ViewStyle;
    action: ViewStyle;
}

export const styles = StyleSheet.create<Style>({
    cardPrimary: {
        flex: 1,
        borderRadius: layout.borderRadius,
        backgroundColor: background.secondary,
    },
    action: {
        flex: 2,
        alignItems: 'center',
    },
});

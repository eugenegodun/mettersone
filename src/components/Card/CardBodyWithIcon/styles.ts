import { ImageStyle, StyleSheet, ViewStyle } from 'react-native';
import { layout } from '../../../constants/styles';

interface Style {
    container: ViewStyle;
    imageContainer: ViewStyle;
    textBlock: ViewStyle;
}

export const styles = StyleSheet.create<Style>({
    container: {
        marginTop: -layout.space,
        flex: 1,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    imageContainer: {
        flex: 2,
    },
    textBlock: {
        flex: 1,
        display: 'flex',
        justifyContent: 'flex-end',
    },
});

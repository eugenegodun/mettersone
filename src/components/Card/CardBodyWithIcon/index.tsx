import React, { ReactElement } from 'react';
import { ImageBackground, Text, View } from 'react-native';

import { CardBody } from '../CardBody';
import { styles } from './styles';

interface OuterProps {
    source: HTMLImageElement;
    children: ReactElement;
    width: number;
    height: number;
}

export const CardBodyWithIcon = ({ source, children, width, height }: OuterProps) => (
    <CardBody>
        <View style={styles.container}>
            <View style={styles.imageContainer}>
                <ImageBackground style={{ height, width }} source={source}></ImageBackground>
            </View>
            <View style={styles.textBlock}>{children}</View>
        </View>
    </CardBody>
);

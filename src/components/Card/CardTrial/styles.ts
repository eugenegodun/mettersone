import { StyleSheet, TextStyle, ViewStyle } from 'react-native';
import { background, color, layout } from '../../../constants/styles';

interface Style {
    inner: ViewStyle;
    gradient: ViewStyle;
    textDanger: TextStyle;
}

export const styles = StyleSheet.create<Style>({
    inner: {
        flex: 1,
    },
    gradient: {
        flex: 1,
        borderRadius: layout.borderRadius,
    },
    textDanger: {
        color: color.danger,
    },
});

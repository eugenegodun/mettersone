import LinearGradient from 'react-native-linear-gradient';
import React from 'react';
import { View } from 'react-native';
import { compose, withHandlers } from 'recompose';

import { gradient } from '../../../constants/styles';
import { CardBodyWithIcon } from '../CardBodyWithIcon';
import { CardButton } from '../CardButton';
import { CardContainer } from '../CardContainer';
import { CardFooter } from '../CardFooter';
import { CardHeader } from '../CardHeader';
import { CardCore } from '../CardCore';
import { CardText } from '../CardText';
import { CardTitle } from '../CardTitle';
import { styles } from './styles';

interface InnerProps {
    onPressButton: () => void;
}

const CardTrialDumb = ({ onPressButton }: InnerProps) => (
    <CardContainer>
        <View style={styles.inner}>
            <LinearGradient style={styles.gradient} colors={gradient.trial}>
                <CardCore>
                    <>
                        <CardHeader>
                            <CardTitle type="light">Trial period ends soon</CardTitle>
                        </CardHeader>
                        <CardBodyWithIcon
                            source={require('../../../assets/images/bulb.png')}
                            width={49}
                            height={80}
                        >
                            <CardText type="light">
                                In 2 days you will not be able to plan. All your data is safe. No
                                worries.
                            </CardText>
                        </CardBodyWithIcon>
                        <CardFooter type="light">
                            <CardButton
                                type="light"
                                onPress={onPressButton}
                                title="Buy subscription"
                            />
                        </CardFooter>
                    </>
                </CardCore>
            </LinearGradient>
        </View>
    </CardContainer>
);

export const CardTrial = compose<InnerProps, {}>(
    withHandlers({
        onPressButton: () => () => console.log('onPressButton'),
    }),
)(CardTrialDumb);

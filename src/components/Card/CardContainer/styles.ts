import { StyleSheet, ViewStyle } from 'react-native';
import { layout } from '../../../constants/styles';

interface Style {
    container: ViewStyle;
}

export const styles = StyleSheet.create<Style>({
    container: {
        flexDirection: 'column',
        marginBottom: 15,
        height: (layout.window.width - 30) / 1.1,
        // shadowColor: '#000',
        // shadowOffset: {
        //     width: 0,
        //     height: 16,
        // },
        // shadowOpacity: 0.08,
        // shadowRadius: 16,
        // elevation: 16,
    },
});

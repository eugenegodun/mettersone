import React, { ReactElement } from 'react';
import { View } from 'react-native';
import { styles } from './styles';

interface OuterProps {
    children: ReactElement;
}

export const CardContainer = ({ children }: OuterProps) => (
    <View style={styles.container}>{children}</View>
);

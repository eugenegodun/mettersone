import { StyleSheet, TextStyle } from 'react-native';

import { color, layout } from '../../../constants/styles';

interface Style {
    text: TextStyle;
    light: TextStyle;
    dark: TextStyle;
}

export const styles = StyleSheet.create<Style>({
    text: {
        marginTop: layout.space,
        fontSize: 16,
        lineHeight: 20,
    },
    light: {
        color: color.secondary,
    },
    dark: {
        color: color.primary,
    },
});

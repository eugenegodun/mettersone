import React from 'react';
import { Text, TextStyle } from 'react-native';
import { styles } from './styles';
import { ThemeType } from '../../../types';

interface OuterProps {
    children: string;
    type: ThemeType;
    style?: TextStyle;
}

export const CardText = ({ children, type, style }: OuterProps) => (
    <Text style={[styles.text, style, styles[type]]}>{children}</Text>
);

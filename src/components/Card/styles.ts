import { StyleSheet, ViewStyle } from 'react-native';
import { layout } from '../../constants/styles';

interface Style {
    box: ViewStyle;
}

export const styles = StyleSheet.create<Style>({
    box: {
        flex: 1,
        padding: layout.space,
    },
});

import React, { ReactElement } from 'react';
import { View } from 'react-native';
import { CardBox } from '../CardBox';
import { styles } from './styles';

interface OuterProps {
    children: ReactElement;
}

export const CardBody = ({ children }: OuterProps) => (
    <View style={styles.body}>
        <CardBox style={styles.box}>{children}</CardBox>
    </View>
);

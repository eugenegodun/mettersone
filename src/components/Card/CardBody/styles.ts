import { StyleSheet, ViewStyle } from 'react-native';

interface Style {
    body: ViewStyle;
    box: ViewStyle;
}

export const styles = StyleSheet.create<Style>({
    body: {
        flex: 3,
    },
    box: {
        justifyContent: 'flex-end',
    },
});

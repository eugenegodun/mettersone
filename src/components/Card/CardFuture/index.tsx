import React from 'react';
import { View } from 'react-native';
import { NavigationScreenProps, withNavigation } from 'react-navigation';

import { CardBodyWithIcon } from '../CardBodyWithIcon';
import { CardButton } from '../CardButton';
import { CardContainer } from '../CardContainer';
import { CardFooter } from '../CardFooter';
import { CardHeader } from '../CardHeader';
import { CardCore } from '../CardCore';
import { CardText } from '../CardText';
import { CardTitle } from '../CardTitle';
import { styles } from './styles';

const CardFutureDumb = ({ navigation }: NavigationScreenProps) => {
    const onPressHandler = () => {
        navigation.navigate('AddNew');
    };
    return (
        <CardContainer>
            <View style={styles.inner}>
                <CardCore>
                    <>
                        <CardHeader>
                            <CardTitle type="dark">Tomorrow</CardTitle>
                        </CardHeader>
                        <CardBodyWithIcon
                            source={require('../../../assets/images/heart.png')}
                            width={96}
                            height={96}
                        >
                            <CardText type="dark">
                                What is the one and only one thing you want to do tomorrow?
                            </CardText>
                        </CardBodyWithIcon>
                        <CardFooter type="dark">
                            <CardButton type="dark" title="Plan now" onPress={onPressHandler} />
                        </CardFooter>
                    </>
                </CardCore>
            </View>
        </CardContainer>
    );
};

export const CardFuture = withNavigation(CardFutureDumb);

import { StyleSheet, TextStyle, ViewStyle } from 'react-native';
import { background, color, layout } from '../../../constants/styles';

interface Style {
    inner: ViewStyle;
    textDanger: TextStyle;
}

export const styles = StyleSheet.create<Style>({
    inner: {
        flex: 1,
        borderRadius: layout.borderRadius,
        backgroundColor: background.primary,
        borderColor: color.danger,
        borderWidth: 1,
    },
    textDanger: {
        color: color.danger,
    },
});

import { StyleSheet, TextStyle, ViewStyle } from 'react-native';
import { background, color, font, layout } from '../../constants/styles';

interface Style {
    button: ViewStyle;
}

export const styles = StyleSheet.create<Style>({
    button: {
        position: 'absolute',
        top: layout.space,
        right: layout.space,
    },
});

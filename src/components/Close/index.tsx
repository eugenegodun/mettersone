// import { Ionicons } from '@expo/vector-icons';
import React from 'react';
import { TouchableOpacity } from 'react-native';
import { NavigationScreenProps, withNavigation } from 'react-navigation';
import { compose, withHandlers } from 'recompose';

import { styles } from './styles';
import { color } from '../../constants/styles';

interface InnerProps {
    onPressHandler: () => void;
    navigation: NavigationScreenProps;
}

const CloseDumb = ({ onPressHandler }: InnerProps) => (
    <TouchableOpacity onPress={onPressHandler} style={styles.button}>
        {/* <Ionicons name="ios-close-circle-outline" size={32} color={color.secondary} /> */}
    </TouchableOpacity>
);

export const Close = compose<InnerProps, {}>(
    withNavigation,
    withHandlers({
        onPressHandler: ({ navigation }: NavigationScreenProps) => () => {
            navigation.navigate('TimeLine');
        },
    }),
)(CloseDumb);

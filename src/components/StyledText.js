import React from "react";
import { Text } from "react-native";
import { shape } from "prop-types";

export const MonoText = props => (
    <Text {...props} style={[props.style, { fontFamily: "space-mono" }]} />
);

MonoText.propTypes = {
    style: shape({}).isRequired,
};

import React, { useEffect, useState } from 'react';
import { KeyboardAvoidingView, TextInput, View } from 'react-native';
import { NavigationScreenProps, withNavigation } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from 'react-native-firebase';

import { color } from '../../constants/styles';
import { CardButton } from '../Card/CardButton';
import { CardContainer } from '../Card/CardContainer';
import { CardFooter } from '../Card/CardFooter';
import { CardHeader } from '../Card/CardHeader';
import { CardTitle } from '../Card/CardTitle';
import { styles } from './styles';
import { FullScreenLoader } from '../FullScreenLoader';

const AddNewDumb = ({ navigation }: NavigationScreenProps) => {
    const [text, setText] = useState('');
    const [userId, setUserId] = useState(null);
    useEffect(() => {
        AsyncStorage.getItem('userData').then(userDataJson => {
            const id = JSON.parse(userDataJson!).uid;
            setUserId(id);
        });
    }, []);
    const onPressHandler = async () => {
        const timestamp = Number(new Date());
        await firebase
            .database()
            .ref()
            .child(`cards/${userId}/${timestamp}`)
            .set({
                date: timestamp,
                id: timestamp,
                body: text,
                title: 'I will',
                indexOn: 'date',
                done: false,
            });
        navigation.navigate('TimeLine');
    };
    if (!userId) {
        return <FullScreenLoader />;
    }
    return (
        <KeyboardAvoidingView
            behavior="padding"
            style={styles.container}
            keyboardVerticalOffset={70}
        >
            <CardContainer>
                <View style={styles.inner}>
                    <CardHeader>
                        <CardTitle type="dark">I will ...</CardTitle>
                    </CardHeader>
                    <TextInput
                        placeholder="Type only one thing that matters for you… E.g. “Call my mom”"
                        placeholderTextColor={color.primaryLight}
                        onChangeText={setText}
                        value={text}
                        maxFontSizeMultiplier={10}
                        autoFocus
                        multiline
                        style={styles.input}
                    />
                    <CardFooter type="dark">
                        <CardButton onPress={onPressHandler} type="danger" title="Done" />
                    </CardFooter>
                </View>
            </CardContainer>
        </KeyboardAvoidingView>
    );
};

export const AddNew = withNavigation(AddNewDumb);

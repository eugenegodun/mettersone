import React from 'react';
// import * as Icon from '@expo/vector-icons';
import { string, bool } from 'prop-types';

import { color } from '../constants/styles';

const TabBarIcon = props => (
    {/* <Icon.Ionicons
        name={props.name}
        size={26}
        style={{ marginBottom: -3 }}
        color={props.focused ? color.tabIconSelected : color.tabIconDefault}
    /> */}
);

TabBarIcon.propTypes = {
    name: string.isRequired,
    focused: bool.isRequired,
};

export default TabBarIcon;

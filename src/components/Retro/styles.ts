import { StyleSheet, TextStyle, ViewStyle } from 'react-native';
import { background, color, font, layout } from '../../constants/styles';

interface Style {
    container: ViewStyle;
    inner: ViewStyle;
    input: TextStyle;
    textDanger: TextStyle;
    footer: TextStyle;
}

export const styles = StyleSheet.create<Style>({
    container: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    inner: {
        flex: 2,
        borderTopRightRadius: layout.borderRadius,
        borderTopLeftRadius: layout.borderRadius,
        backgroundColor: background.primary,
    },
    textDanger: {
        color: color.danger,
    },
    input: {
        flex: 3,
        padding: layout.space,
        justifyContent: 'flex-start',
        fontSize: font.size.default,
    },
    footer: {
        flex: 3,
        padding: layout.space,
        justifyContent: 'flex-start',
        fontSize: font.size.default,
    },
});

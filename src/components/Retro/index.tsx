import React, { useState, useEffect } from 'react';
import { KeyboardAvoidingView, TextInput, View } from 'react-native';
import { NavigationScreenProps, withNavigation } from 'react-navigation';
import firebase from 'react-native-firebase';

import { color } from '../../constants/styles';
import { CardButton } from '../Card/CardButton';
import { CardContainer } from '../Card/CardContainer';
import { CardFooter } from '../Card/CardFooter';
import { CardHeader } from '../Card/CardHeader';
import { CardTitle } from '../Card/CardTitle';
import { styles } from './styles';

const RetroDumb = ({ navigation }: NavigationScreenProps) => {
    const [text, setText] = useState('');
    const userId = navigation.getParam('userId');
    const cardId = navigation.getParam('cardId');
    const fetchCard = async () => {
        await firebase
            .database()
            .ref()
            .child(`cards/${userId}/${cardId}`)
            .on('value', snap => {
                const { retro } = snap.val();
                if (retro) {
                    setText(retro);
                }
            });
    };
    useEffect(() => {
        console.log('fetchCard');
        fetchCard();
    }, []);
    const onPressHandler = async () => {
        await firebase
            .database()
            .ref()
            .child(`cards/${userId}/${cardId}`)
            .update({
                success: false,
                done: true,
                retro: text,
            });
        navigation.navigate('TimeLine');
    };
    return (
        <KeyboardAvoidingView
            behavior="padding"
            style={styles.container}
            keyboardVerticalOffset={70}
        >
            <CardContainer>
                <View style={styles.inner}>
                    <CardHeader>
                        <CardTitle type="dark">I failed because ...</CardTitle>
                    </CardHeader>
                    <TextInput
                        placeholder="Type the reason of your failure to help your-self progress"
                        placeholderTextColor={color.primaryLight}
                        onChangeText={setText}
                        value={text}
                        maxFontSizeMultiplier={10}
                        autoFocus
                        multiline
                        style={styles.input}
                    />
                    <CardFooter type="dark">
                        <CardButton onPress={onPressHandler} type="danger" title="Done" />
                    </CardFooter>
                </View>
            </CardContainer>
        </KeyboardAvoidingView>
    );
};

export const Retro = withNavigation(RetroDumb);
